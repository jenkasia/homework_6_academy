import React, { useEffect, useRef } from 'react';
import Message from './Message';
import IMessage from './Interfaces/IMessage';

type ChatProps = {
  messages: IMessage[];
  currentUserId: string;
  likedMessagesList: Array<string>;
  onRemoveMessage(messageID: string): void;
  onLikeDislikeMessage(messageID: string): void;
  onEditMessage(messageID: string): void;
};

const ChatBody: React.FC<ChatProps> = (props) => {
  const checkIsCurrentUser = (
    userID: string,
    currentMessageUserId: string
  ): boolean => {
    return userID === currentMessageUserId;
  };

  const parseDate = (dateString: string): string => {
    let date = new Date(dateString);
    let time = `${date.getHours()}:${date.getMinutes()}`;
    return time;
  };

  const messagesEndRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = () => {
    messagesEndRef.current!.scrollIntoView!({ behavior: 'smooth' });
  };

  useEffect(scrollToBottom, [props.messages]);

  return (
    <div className='chat__body'>
      {props.messages
        .sort((b, a) => {
          return Date.parse(a.createdAt) - Date.parse(b.createdAt);
        })
        .map((message) => {
          console.log(props.currentUserId === message.userId);
          return (
            <Message
              key={message.id}
              messageType={
                checkIsCurrentUser(props.currentUserId, message.userId)
                  ? 'personal'
                  : ''
              }
              messageID={message.id}
              messageText={message.text}
              imageSrc={message.avatar}
              userName={message.user}
              onRemoveMessage={props.onRemoveMessage}
              onEditMessage={props.onEditMessage}
              onLikeDislikeMessage={props.onLikeDislikeMessage}
              messageData={parseDate(message.createdAt)}
              likedMessagesList={props.likedMessagesList}
            ></Message>
          );
        })
        .reverse()}
      <div ref={messagesEndRef} />
    </div>
  );
};

export default ChatBody;
