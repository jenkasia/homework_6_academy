import React from 'react';

type HeaderChatProps = {
  chatName: string;
  participants: number;
  messagesAmount: number;
  lastMessageDate: string;
};

const ChatHeader: React.FC<HeaderChatProps> = (props) => {
  const parseDate = (dateString: string): string => {
    let date = new Date(dateString);
    let time = `${date.getHours()}:${date.getMinutes()}`;
    return time;
  };

  return (
    <div className='chat__header'>
      <div className='chat__header-left'>
        <p className='chat__header-item'>{props.chatName}</p>
        <p className='chat__header-item'>{props.participants} participants</p>
        <p className='chat__header-item'>{props.messagesAmount} messages</p>
      </div>
      <div className='chat__header-right'>
        <p className='chat__header-item'>
          Last message at {parseDate(props.lastMessageDate)}
        </p>
      </div>
    </div>
  );
};

export default ChatHeader;
