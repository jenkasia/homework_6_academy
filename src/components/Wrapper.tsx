import React, { ReactNode } from 'react';

type WrapperProps = {
  children?: ReactNode;
  class?: string;
};

const Wrapper: React.FC<WrapperProps> = (props) => {
  const classes = ['wrapper'];
  if (props.class) {
    classes.push(props.class);
  }
  return <div className={classes.join(' ')}>{props.children}</div>;
};

export default Wrapper;
