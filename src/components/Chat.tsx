import React from 'react';
import Wrapper from './Wrapper';
import ChatHeader from './ChatHeader';
import ChatBody from './ChatBody';
import ChatForm from './ChatForm';
import LoaderSpinner from './LoaderSpinner';
import IMessage from './Interfaces/IMessage';

interface IState {
  chanName: string;
  loading: boolean;
  messages: Array<IMessage>;
  currentUserId: string;
  likedMessages: Array<string>;
  isEditingMessage: boolean;
  textToEdit: string;
  idOfEditMessage: string;
}

class Chat extends React.Component {
  state: IState = {
    chanName: 'Binary Studio Chat',
    loading: true,
    messages: [],
    currentUserId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    likedMessages: [],
    isEditingMessage: false,
    textToEdit: '',
    idOfEditMessage: ''
  };

  fetchData = () => {
    console.log('before fetch', this.state.messages);
    fetch(
      'https://gist.githubusercontent.com/jenkasia/25a488a103525a8278c9e7695319c9d0/raw/91d5ed1fdd700aa2f15cf70248f44df1cc4786c5/messages'
    )
      .then((res) => res.json())
      .then((res) =>
        this.setState({
          messages: res || []
        })
      );
    console.log('after fetch', this.state.messages);
    setTimeout(() => this.setState({ loading: false }), 1200);
    console.log('after timer', this.state.messages);
  };

  componentDidMount() {
    this.fetchData();
  }

  getParticipantsCount = (messages: Array<IMessage>): number => {
    console.log('Chat -> messages', messages);
    const participantsRepeating = messages.map((message) => message.userId);
    return new Set(participantsRepeating).size;
  };

  getLastMessageDate = (messages: Array<IMessage>) => {
    return messages.sort((b, a) => {
      return Date.parse(a.createdAt) - Date.parse(b.createdAt);
    })[0].createdAt;
  };

  idGenerator = () => {
    const id = `f${(+new Date()).toString(16)}`;
    return id;
  };

  addMessageHandler = (message: string) => {
    const newMessage: IMessage = {
      id: this.idGenerator(),
      text: message,
      user: '',
      avatar: '',
      userId: this.state.currentUserId,
      editedAt: '',
      createdAt: new Date().toString()
    };
    const newMessageArray = [newMessage, ...this.state.messages];
    this.setState({ messages: newMessageArray });
  };

  editMessageStartHandler = (messageID: string) => {
    const message = this.state.messages.find((message) => message.id === messageID);

    this.setState({
      textToEdit: message!.text,
      isEditingMessage: true,
      idOfEditMessage: messageID
    });
  };

  editMessageHandler = (textMessage: string) => {
    this.setState({
      textToEdit: textMessage
    });
  };

  editMessageSaveHandler = (messageID: string, newMessage: string) => {
    const messageIndex = this.state.messages.findIndex((message) => messageID === message.id);
    const updatedMessage = this.state.messages[messageIndex];
    updatedMessage.editedAt = new Date().toString();
    updatedMessage.text = newMessage;
    const newMessageArray = this.state.messages
      .slice(0, messageIndex)
      .concat(updatedMessage, this.state.messages.slice(messageIndex + 1, this.state.messages.length));
    this.setState({
      messages: newMessageArray,
      isEditingMessage: false,
      textToEdit: '',
      idOfEditMessage: ''
    });
  };

  removeMessageHandler = (messageID: string) => {
    console.log(this.state);
    const messageIndex = this.state.messages.findIndex((message) => messageID === message.id);
    console.log('Chat -> removeMessageHandler -> messageIndex', messageIndex);
    const newMessageArray = this.state.messages
      .slice(0, messageIndex)
      .concat(this.state.messages.slice(messageIndex + 1, this.state.messages.length));
    console.log('Chat -> removeMessageHandler -> messageIndex', messageIndex);
    console.log('Chat -> removeMessageHandler -> newArray', newMessageArray);
    this.setState({ messages: newMessageArray });
  };

  toggleLike = (messageID: string) => {
    console.log('Chat -> toggleLike -> messageID', messageID);
    console.log(this.state);
    let uniqueLikes = new Set(this.state.likedMessages);
    if (uniqueLikes.has(messageID)) {
      uniqueLikes.delete(messageID);
    } else {
      uniqueLikes.add(messageID);
    }
    const newArray = Array.from(uniqueLikes);
    this.setState({ likedMessages: newArray });
  };

  render() {
    const { loading } = this.state;
    return loading ? (
      <LoaderSpinner></LoaderSpinner>
    ) : (
      <Wrapper class='chat'>
        <ChatHeader
          participants={this.getParticipantsCount(this.state.messages)}
          messagesAmount={this.state.messages.length}
          lastMessageDate={this.getLastMessageDate(this.state.messages)}
          chatName={this.state.chanName}
        />
        <ChatBody
          messages={this.state.messages}
          currentUserId={this.state.currentUserId}
          onRemoveMessage={this.removeMessageHandler}
          onLikeDislikeMessage={this.toggleLike}
          onEditMessage={this.editMessageStartHandler}
          likedMessagesList={this.state.likedMessages}
        />
        {this.state.isEditingMessage ? (
          <ChatForm
            onSaveMessage={this.editMessageSaveHandler}
            idOfEditMessage={this.state.idOfEditMessage}
            textToEdit={this.state.textToEdit}
            onEditMessage={this.editMessageHandler}
          ></ChatForm>
        ) : (
          <ChatForm onAdd={this.addMessageHandler}></ChatForm>
        )}
      </Wrapper>
    );
  }
}
export default Chat;
