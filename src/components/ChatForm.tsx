import React, { useRef } from 'react';

interface ChatFormProps {
  onAdd?(message: string): void;
  onSaveMessage?(messageID: string, message: string): void;
  onEditMessage?(messageID: string): void;
  idOfEditMessage?: string;
  textToEdit?: string;
}

const ChatForm: React.FC<ChatFormProps> = (props) => {
  const ref = useRef<HTMLTextAreaElement>(null);

  const onSubmitHandlerToSend = (event: React.FormEvent) => {
    event.preventDefault();
    const textMessage = ref.current!.value.trim();
    if (textMessage) {
      props.onAdd!(ref.current!.value);
      ref.current!.value = '';
    }
  };

  const onSubmitHandlerToSave = (event: React.FormEvent) => {
    event.preventDefault();
    const textMessage = ref.current!.value.trim();
    if (textMessage) {
      props.onSaveMessage!(props.idOfEditMessage!, ref.current!.value);
      ref.current!.value = '';
    }
  };

  const onSubmitHandlerToEdit = (event: React.FormEvent) => {
    event.preventDefault();
    const textMessage = ref.current!.value.trim();
    if (textMessage) {
      props.onEditMessage!(textMessage);
    }
  };

  return (
    <form
      className='chat__form'
      onSubmit={
        props.textToEdit ? onSubmitHandlerToSave : onSubmitHandlerToSend
      }
    >
      {props.textToEdit ? (
        <textarea
          className='form_input'
          id='story'
          name='story'
          ref={ref}
          value={props.textToEdit}
          onChange={onSubmitHandlerToEdit}
        ></textarea>
      ) : (
        <textarea
          className='form_input'
          id='story'
          name='story'
          ref={ref}
        ></textarea>
      )}

      <button type='submit' className='form_button'>
        Send
      </button>
    </form>
  );
};

export default ChatForm;
